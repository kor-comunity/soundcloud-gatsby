module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "soundcloud-gatsby",
  },
  plugins: ["gatsby-plugin-gatsby-cloud"],
};
