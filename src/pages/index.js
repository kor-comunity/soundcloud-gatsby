import React, { useState, useEffect } from 'react';
import {
  useGoogleReCaptcha
} from 'react-google-recaptcha-v3';
const axios = require('axios').default;



// styles
const pageStyles = {
  color: "#232129",
  padding: 96,
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}

const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
}

const headingAccentStyles = {
  color: "#663399",
}

// markup
const IndexPage = () => {  
  const [chombo, setChombo] = useState(null);    

  useEffect(() => {        
    var config = {
      method: 'get',
      url: 'https://josen.korconnect.io/Spotify/artists/6iSZjc4kOoKZKiBXK5HbwD',
      headers: { 
        'Accept': 'application/json',
        'x-api-key': '5ccwrVXAJR8JR4NOz4ON972VKyE34GZK7poT31NK'  
      }
    };

    axios(config)
    .then(function (response) {
      console.log(response.data)
      setChombo(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });    
  }, []);

  return (
    <main style={pageStyles}>
      <title>KOR Connect Demo</title>

      <h1 style={headingStyles}>
        KOR Connect<br />
        <span style={headingAccentStyles}>
          + Spotify + Gatsby
        </span>
      </h1>      
      { chombo ? (
        <div>
          <img alt="chombo" width="250" src={chombo.images[0].url} />
          <h2><a href={chombo.external_urls}>{chombo.name}</a></h2>            
          <h3>Popularity &nbsp;{chombo.popularity}</h3>
        </div>
      ) : (
        <h3>fetching data...</h3>
      )}
    </main>
  )
}

export default IndexPage
